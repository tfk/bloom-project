set title "Inserted elements versus runtime"
set xlabel "Number of inserted elements"
set ylabel "Runtime (seconds)"
plot "table.tsv" using 1:3 with linespoints title "Standard BF Runtime", "table.tsv" using 1:7 with linespoints title "Blocked BF Runtime", "table.tsv" using 1:10 with linespoints title "Dynamic BF Runtime";
set terminal unknown
