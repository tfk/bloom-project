set title "Dynamic Blocked BF False Positive Rate"
set xlabel "Number of Hash Functions"
set ylabel "False Positive Rate (Percent)"
set log x 2
set log y 2
set xlabel font ",14"
set ylabel font ",14"
set key font ",12"
set xtics font ",11"
set ytics font ",11"
set title font ",16"
plot "table_64_0" using 2:3 with linespoints title "Standard BF FP", "table_64_1" using 2:3 with linespoints title "Blocked BF FP", "table_64_2" using 2:3 with linespoints title "Dynamic Blocked BF FP";
