\secput{scalablebloom}{Scalable Bloom Filters}

A scalable bloom filter \cite{AlmeidaBa07} is a bloom filter that can resize itself
dynamically while maintaining an upper bound on its false positive rate.  We
first present the design and analysis of a scalable bloom filter and then
provide empirical results using our implementation to demonstrate its false positive
rate and space usage.

\subheading{Design and Analysis}

A scalable bloom filter (or dynamic bloom filter) $X_{D}$ is implemented as a
chain of standard bloom filters $X_{S,1}, \ldots X_{S,t}$ whose false positive
probabilities decrease geometrically according to a \defn{tightening ratio} $r$.
If the false positive probability of $X_{S,1}$ is $p$, then the false positive probability of
the $i^{th}$ filter in the chain is $r^{i-1}p$. 

Elements inserted into $X_{D}$ are added to the last filter in the chain,
$X_{S,t}$. When $X_{S,t}$ is unable to insert additional elements without
sacrificing its false positive probability a new standard bloom filter $X_{S,t+1}$ is
added to the chain. The capacity for newly added bloom filters can be chosen
independently of its false positive probability. It is common, however, for the sizes
of each filter in the chain to grow exponentially to allow the scalable filter
to grow arbitrarily large while guaranteeing a logarithmic chain length.

To perform an approximate membership query on $X_{D}$ a query is
performed on each filter in the chain $X_{S,1},\ldots X_{S,t}$. The query
reports that the element is in the set if any filter in the chain reports that
it contains the element and returns \const{false} otherwise. The probability of a false
positive, therefore, is bounded from above by the probability of any filter in the chain
reporting a false positive. 

\begin{align*}
Pr(\textit{False positive in $X_{D}$}) &< \sum_{i=1}^{t} r^{i-1}p\\
&< \frac{1}{1-r} p
\end{align*}

\subheading{Tightening false positive rate}

In order to tighten the false positive rate, we recall that a standard bloom
filter $X_{S}(n,c,k)$ has a minimal false positive rate when $c = k/ \ln 2$
that is equal to $(1-e^{-k/c})^{k} = (1/2)^{k}$. If the standard bloom filter
has a false positive rate of $p = (1/2)^{k}$, then a new standard bloom filter
$X_{S}(n',c',k')$ with a false positive rate of $p' = rp$ can be obtained by
setting $k' = k + \lg(1/r)$, and $c' = k\ln 2$.  Then $p' = (1/2)^{k + \lg
(1/r)} = rp$. For our implementations we choose $r = 1/2$ so that $k' = k+1$,
and $c' = c + \ln 2$. We utilize floating point representations of $k$ and $c$
to reduce the impact of rounding errors.

\subheading{Initializing a scalable bloom filter}

Suppose that we wish to initialize a scalable bloom filter that maintains the
same false positive probability of a standard bloom filter that knows its size in
advance: $X_{S}(n,c,k)$. Suppose that the false positive probability of $X_{S}$ is
$q$. The bound on the scalable bloom filter's false positive rate shows that to
guarantee a false positive rate of $q$ the false positive probability of the
first bloom filter in the chain must be $p = (1-r)q$. As we saw when we
tightened the false probability rate of each filter in the chain, this requires us to
increase $k$ by $\lg(1/(1-r))$ and the number of bits per element by $\lg
(1/(1-r))/0.7$.  For example, if $r = 1/2$ this, then we increase $k$ by $2$
and the number of bits per element by $2/0.7 \approx 2.85$. The first bloom
filter in the chain, therefore, will be $X_{S,1}(n',c+1.42, k+1)$ where $n'$ is
an initial estimate of the size of the set.

\subheading{Implementation, and empirical results}

\begin{figure}
\begin{minipage}[ht]{0.60\textwidth}
\vspace{1ex}
  \footnotesize
 \includegraphics[scale=0.40]{figs/scalable_bloom_filter_memory_use.eps}
\end{minipage}
\hspace{40pt}
\begin{minipage}[ht]{0.40\textwidth}
  \caption{Plot of the memory use (in MB) for scalable and
standard bloom filters as a function of the number of hash functions utilized. 
Results for the scalable bloom filter are shown for two
initial set sizes: 512 elements and 2048 elements.
  }   \label{fig:scalable-bf-memory} \end{minipage}
\end{figure}

\begin{figure}
\begin{minipage}[ht]{0.60\textwidth}
\vspace{1ex}
  \footnotesize
  \centering
 \includegraphics[scale=0.40]{figs/scalable_bloom_filter_false_positive_rate.eps}
\end{minipage}
\hspace{40pt}
\begin{minipage}[ht]{0.40\textwidth}
  \caption{Plot of the false positive rate for scalable and
standard bloom filters as a function of the number of hash functions utilized. The x-axis is on
a log (base 2) scale. Results for the scalable bloom filter are shown for two
initial set sizes: 512 elements and 2048 elements.
  }
  \label{fig:scalable-bf-fp} \end{minipage}
\end{figure}

I ran an experiment to compare the false positive rate and memory usage of the
standard and scalable bloom filters. A total of $n = 2^{25}$ random elements
(pseudorandom integers) were inserted into each bloom filter. Then $2n$ lookups
were performed $n$ of which were guaranteed to have been inserted into the set.
An exact set data structure containing all inserted elements was used to
compute the exact number of false positives. The standard bloom filter was
initialized assuming a set of size $n$, but the scalable bloom filters used
smaller intial set sizes of $512$ and $2048$. The tightening ratio for the
scalable bloom filters was chosen to be $r = 1/2$ and the capacity of $X_{S,i+1}$
was set to be double the capacity of $X_{S,i}$, bounding the length of the chain
by $\lg n$. 

The result of the experiment measuring memory usage is displayed in
\figref{scalable-bf-memory}. Note that the initial size of the bloom filter can
have a big impact on memory usage. The scalable bloom filter intially sized to
contain $512$ elements uses roughly double the space of the scalable bloom
filter initially sized to contain $2048$ elements.  The reason for this
increased space usage is that each bloom filter in the chain requires
additional bits for each inserted element to guarantee a tightened false
positive rate.  

An additional phenomenon we note is that the initial size of the scalable bloom
filter appears to have an impact on its false positive rates in practice. A
comparison of the scalable bloom filters initialized with $512$ and $2048$
elements with the standard bloom filter in \figref{scalable-bf-fp} reveals that
the filter with initial capacity of $2048$ elements matches the false positive
rate of the standard bloom fitler for larger values of $k$. The false positive
rate of the filter with initial capacity $512$ grows larger than that of the
standard bloom filter when more than $12$ hash functions are used. The filter
with initial capacity $2048$, however, has a false positive rate that is lower
than the standard bloom filter until $15$ hash functions are used. It is, of
course, expected that the inital capacity would have some effect on the false
positive rate because the false positive rate of a scalable bloom filter
experiencing $t$ resize operations will be the sum of $t$ terms in a geometric series
converging to its desired probability. The magnitude of the effect,
however, is somewhat surprising. The original work on scalable bloom filters
in \cite{AlmeidaBa07} did not provide a direct empirical comparison of the
false positive rates of scalable and standard bloom filters. For this reason,
it is unclear whether this phenomenon is due to a subtle implementation bug
(perhaps a rounding error) or has some other cause (perhaps related to the
imprecision of various approximations).



%%% mode: latex
%%% TeX-master: "proposal"
%%% End: 

% LocalWords:  PageRank PageRanks vertices GraphLab parallelize parallelizing
% LocalWords:  nondeterministic nondeterminism runtime parallelized Seidel MIS
% LocalWords:  nonatomic parallelizes superlinear Luby's fluidanimate Plassmann
% LocalWords:  determinize multicore

