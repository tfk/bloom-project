set title "Runtime versus Set Size"
set xlabel "Set Size (Number of inserted elements)"
set ylabel "Runtime (seconds)"
plot "runtime_alg0.tsv" using 1:3 with linespoints title "Standard BF", "runtime_alg1.tsv" using 1:3 with linespoints title "Blocked BF", "runtime_alg2.tsv" using 1:3 with linespoints title "Scalable Blocked BF";
