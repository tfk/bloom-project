set title "Scalable Bloom Filter Memory Use"
set xlabel "Number of Hash Functions"
set ylabel "Memory Use (MB)"
set xlabel font ",14"
set ylabel font ",14"
set yrange [0:220]
set key font ",12"
set xtics font ",11"
set ytics font ",11"
set title font ",16"
plot "scalability_test_alg0.tsv" using 2:4 with linespoints title "Standard BF", "scalability_test_alg3.tsv" using 2:4 with linespoints title "Scalable BF (initial size 2048)", "scalability_test_alg3_512.tsv" using 2:4 with linespoints title "Scalable BF (initial size 512)";
