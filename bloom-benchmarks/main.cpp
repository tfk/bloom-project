#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <fstream>
#include <ctime>
#include <set>
#include <cmath>
#include <string>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */
#include <err.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <cilk/cilk.h>
#include <cilk/cilk_stub.h>
#include "utils.h"


/* Structure */
typedef struct simple_bloom_filter {
  char* words;
  uint32_t size;
  uint8_t k;
} simple_bloom_filter;

typedef struct sliced_bloom_filter {
  char* words;
  uint32_t k;
  uint32_t slice_size; // total size is slice_size * k.
} sliced_bloom_filter;

typedef struct scalable_bloom_filter {
  scalable_bloom_filter* next_filter;
  uint32_t current_size;
  uint32_t size_hint;
  float f_k;
  simple_bloom_filter filter;
} scalable_bloom_filter;

typedef struct cache_efficient_bloom_filter {
  simple_bloom_filter* filters;
  uint64_t size;
  uint64_t num_filters;
  uint8_t k;
} cache_efficient_bloom_filter;

typedef struct cache_efficient_scalable_bloom_filter {
  scalable_bloom_filter* filters;
  uint64_t size;
  uint64_t num_filters;
  uint8_t k;
} cache_efficient_scalable_bloom_filter;

/* Static Variables */
static bool print_human_readable = true;
static int num_files = 0;
static uint64_t total_allocated = 0;
static float log_prob = 2;
static int total_needed_to_resize = 0;
static float resize_ratio = 1;
static uint64_t BLOCK_SIZE = 64;

void free_wrapper(void* p) {
  // Does nothing, currently.
}

void* malloc_wrapper(uint64_t n) {
  return malloc(n);
}

void* calloc_wrapper(uint64_t m, uint64_t n) {
  total_allocated += m*n*100;
  return calloc(m,n);
}

void init_simple_bloom_filter(simple_bloom_filter* f, uint64_t expected_size, float bits_per_element, float k) {
  f->size = expected_size * bits_per_element;
  int bytes = 1 + f->size / (sizeof(char)*8);
  // round up to cache line.
  bytes = bytes + bytes%64;
  f->words = (char*) calloc_wrapper(sizeof(char), bytes);
  f->k = k;
}

void init_sliced_bloom_filter(sliced_bloom_filter* f, uint64_t expected_size, int bits_per_element, int k) {
  f->slice_size = 1 + expected_size * bits_per_element / k;
  f->k = k;
  f->words = (char*) calloc_wrapper(sizeof(char),1+(f->slice_size * k) / (sizeof(char) * 8));
}

void init_scalable_bloom_filter(scalable_bloom_filter* f, uint64_t size_hint, float bits_per_element, float k) {
  f->current_size = 0;
  f->size_hint = size_hint;
  f->next_filter = NULL;
  f->f_k = k;
  init_simple_bloom_filter(&f->filter, size_hint, bits_per_element, k);
}

static float SCALABLE_FILTER_RESIZE_RATE = 2;
static bool FIXED_RESIZE = false;
void resize_scalable_bloom_filter(scalable_bloom_filter* f) {
  //printf("resizing\n");
  f->next_filter = (scalable_bloom_filter*)malloc(sizeof(scalable_bloom_filter));
  scalable_bloom_filter* next_filter = f->next_filter;
  int new_size = f->current_size*SCALABLE_FILTER_RESIZE_RATE;
  if (FIXED_RESIZE) {
    new_size = BLOCK_SIZE*0.1;
  }
  if (new_size < 64) new_size = 64;
  init_scalable_bloom_filter(next_filter, new_size, (f->f_k + log_prob)/0.7, f->f_k+log_prob);
  //next_filter->f_k += log_prob;
}

void set_bit(char* words, uint64_t bit_index) {
  words[bit_index/8] = words[bit_index/8] | (char)(((char)1) << (bit_index % (sizeof(char) * 8)));
}

void set_bit_atomic(char* words, uint64_t bit_index) {
  __sync_fetch_and_or(&words[bit_index/8], 1 << bit_index % (sizeof(char) * 8));
}

bool get_bit(char* words, uint64_t bit_index) {
  return (words[bit_index/8] & (1 << bit_index % (sizeof(char) * 8)));
}

void insert_simple_bloom_filter(simple_bloom_filter* f, uint64_t element) {
  uint64_t hash1 = utils::hash(element);
  uint64_t hash2 = utils::hash2(element);
  for (int i = 0; i < f->k;  i++) {
    set_bit_atomic(f->words, (hash1+i*hash2) % f->size);
    assert(get_bit(f->words, (hash1+i*hash2) % f->size));
  }
}

void insert_sliced_bloom_filter(sliced_bloom_filter* f, uint64_t element) {
  uint64_t hash1 = utils::hash(element);
  uint64_t hash2 = utils::hash2(element);
  for (int i = 0; i < f->k;  i++) {
    set_bit_atomic(f->words, i*f->slice_size + (hash1+i*hash2) % f->slice_size);
  }
}

bool lookup_simple_bloom_filter(simple_bloom_filter* f, uint64_t element) {
  uint64_t hash1 = utils::hash(element);
  uint64_t hash2 = utils::hash2(element);
  bool found = true;
  for (int i = 0; i < f->k;  i++) {
    if (!get_bit(f->words, (hash1+i*hash2) % f->size)) {
      found = false;
      return false;
    }
  }
  return found;
}

bool lookup_sliced_bloom_filter(sliced_bloom_filter* f, uint64_t element) {
  uint64_t hash1 = utils::hash(element);
  uint64_t hash2 = utils::hash2(element);
  for (int i = 0; i < f->k;  i++) {
    if (!get_bit(f->words, i*f->slice_size + (hash1+i*hash2) % f->slice_size)) {
      return false;
    }
  }
  return true;
}

void insert_scalable_bloom_filter(scalable_bloom_filter* f, uint64_t element) {
  // we need to resize once we've added bits_per_element*size_hint / 2 elements.
  if (f->next_filter == NULL && f->current_size > f->size_hint/*f->size_hint*(1 + ((float)1)/f->k)*/) {
    //total_needed_to_resize++;
    resize_scalable_bloom_filter(f);
  }
  // check if we need to insert into next filter.
  //while (f->chain_length > 0) f = f->next_filter;
  if (f->next_filter != NULL) {
    insert_scalable_bloom_filter(f->next_filter, element);
    return;
  }
  f->current_size++;
  insert_simple_bloom_filter(&(f->filter), element);
}

inline bool lookup_scalable_bloom_filter(scalable_bloom_filter* f, uint64_t element) {
  if (lookup_simple_bloom_filter(&f->filter, element)) {
    return true;
  }
  if (f->next_filter != NULL) return lookup_scalable_bloom_filter(f->next_filter, element);
  return false;
}


bool insert_scalable_bloom_filter_with_overflow(scalable_bloom_filter* f, uint64_t element) {
  // we need to resize once we've added bits_per_element*size_hint / 2 elements.
  if (f->next_filter == NULL && f->current_size > f->filter.size*resize_ratio /*f->size_hint*(1 + ((float)1)/f->k)*/) {
    //printf("resize\n");
    total_needed_to_resize++;
    //f->local_overflow_set.insert(element);
    return true;
    //resize_scalable_bloom_filter(f);
  }
  // check if we need to insert into next filter.
  //while (f->chain_length > 0) f = f->next_filter;
  /*if (f->chain_length > 0) {
    return insert_scalable_bloom_filter_with_overflow(f->next_filter, element);
  }*/
  f->current_size++;
  insert_simple_bloom_filter(&(f->filter), element);
  return true;
}

void init_cache_efficient_bloom_filter(cache_efficient_bloom_filter* f, uint64_t expected_size, float bits_per_element, float k) {
  float space_increase = 1.00;
  f->size = expected_size * space_increase;
  f->k = k;

  //int cache_lines = 16;
//  int cache_lines = 1;

//  f->num_filters = 1 + bits_per_element*f->size / (cache_lines*64*8);
  //printf("Number of filters %d\n", f->num_filters);

  int elements_per_block = 256*(3*k+4)/4;
  //int elements_per_block = 1024;
  BLOCK_SIZE = elements_per_block;
  f->num_filters = f->size / elements_per_block;
  int block_size = elements_per_block;


  //f->filters = (scalable_bloom_filter*) malloc_wrapper(sizeof(scalable_bloom_filter) * f->num_filters);

  f->filters = (simple_bloom_filter*) malloc_wrapper(sizeof(simple_bloom_filter) * f->num_filters);
  for (int i = 0; i < f->num_filters; i++) {
    init_simple_bloom_filter(&f->filters[i], block_size, bits_per_element, k);
  }
}

void init_cache_efficient_scalable_bloom_filter(cache_efficient_scalable_bloom_filter* f, uint64_t expected_size, int bits_per_element, int k) {
  float space_increase = 1.00;
  f->size = expected_size * space_increase;

  f->k = k;

  // minimum number of elements per block.
  //int min_elements_per_block = 64;

  //int cache_lines = 1;
  //f->num_filters = 1 + bits_per_element*f->size / (cache_lines*64*8);

  int elements_per_block = 256*(3*k+4)/4;
  //int elements_per_block = 1024;
  BLOCK_SIZE = elements_per_block;
  //int elements_per_block = 1024;
  f->num_filters = f->size / elements_per_block;
  int block_size = elements_per_block;

  float ratio = 2;

  if (k <= 15) ratio = 1.3;
  //if (k <= 12) ratio = 1.1;
  //if (k <= 10) ratio = 0.3;

  float f_bits = (float)bits_per_element*1.00 + ((float)ratio)/0.7;
  float f_k = (float)f_bits*0.7*1.00 + ratio;

  f->filters = (scalable_bloom_filter*) malloc_wrapper(sizeof(scalable_bloom_filter) * f->num_filters);


  for (int i = 0; i < f->num_filters; i++) {
    //printf("size of scal filter: %d\n", 1+64*8*cache_lines / (bits_per_element));
    init_scalable_bloom_filter(&f->filters[i], block_size, f_bits, f_k);
    f->filters[i].f_k = f_k;
  }

  //init_cache_efficient_bloom_filter(&f->pre_overflow_filter, expected_size/16, bits_per_element/4, k/4);
  //init_cache_efficient_bloom_filter(&f->overflow_filter, expected_size/16, bits_per_element*2, k*2);
  //init_simple_bloom_filter(&f->pre_overflow_filter, expected_size/16, bits_per_element/4, k/4);
  //init_simple_bloom_filter(&f->overflow_filter, expected_size/16, bits_per_element*2, k*2);
}


void init_cache_efficient_bloom_filter_with_sizes(cache_efficient_bloom_filter* f, uint64_t expected_size,
    int bits_per_element, int k, int num_filters, int* sizes) {
  f->size = expected_size;
  f->k = k;
  f->num_filters = num_filters;
  f->filters = (simple_bloom_filter*) malloc_wrapper(sizeof(simple_bloom_filter) * f->num_filters);
  for (int i = 0; i < f->num_filters; i++) {
    init_simple_bloom_filter(&f->filters[i], 8+sizes[i], bits_per_element, k);
  }
}

void insert_cache_efficient_bloom_filter(cache_efficient_bloom_filter* f, uint64_t element) {
  uint64_t filter = (utils::shard(element)) % f->num_filters;
  insert_simple_bloom_filter(&f->filters[filter], element);
}

bool lookup_cache_efficient_bloom_filter(cache_efficient_bloom_filter* f, uint64_t element) {
  uint64_t filter = (utils::shard(element)) % f->num_filters;
  return lookup_simple_bloom_filter(&f->filters[filter], element);
}

void insert_cache_efficient_scalable_bloom_filter(cache_efficient_scalable_bloom_filter* f, uint64_t element) {
  uint64_t filter = (utils::shard(element)) % f->num_filters;
  insert_scalable_bloom_filter(&f->filters[filter], element);
/*  if (!success) {
    insert_simple_bloom_filter(&f->overflow_filter, element);
    //insert_cache_efficient_bloom_filter(&f->overflow_filter, element);
    insert_simple_bloom_filter(&f->pre_overflow_filter, element);
    //insert_cache_efficient_bloom_filter(&f->pre_overflow_filter, element);
  }
*/
  //return success;
}

bool lookup_cache_efficient_scalable_bloom_filter(cache_efficient_scalable_bloom_filter* f, uint64_t element) {
  uint64_t filter = (utils::shard(element)) % f->num_filters;
  bool result = lookup_scalable_bloom_filter(&f->filters[filter], element);
  return result;
  /*if (!result && f->filters[filter].current_size > resize_ratio*f->filters[filter].size_hint
   && lookup_simple_bloom_filter(&f->pre_overflow_filter, element)) return lookup_simple_bloom_filter(&f->overflow_filter, element);*/
  if (!result) {
    return false;
 //f->filters[filter].local_overflow_set.find(element) != f->filters[filter].local_overflow_set.end();
  } else {
    return true;
  }
}

void insert_cache_efficient_bloom_filter_explicit(cache_efficient_bloom_filter* f, uint64_t element, int filter) {
  insert_simple_bloom_filter(&f->filters[filter], element);
}

bool lookup_cache_efficient_bloom_filter_explicit(cache_efficient_bloom_filter* f, uint64_t element, int filter) {
  return lookup_simple_bloom_filter(&f->filters[filter], element);
}


void simple_bloom_filter_test (uint64_t numElements, float bits_per_element, float k, int ratio, bool timing, bool gen_table) {
  //printf("Begin simple bloom filter test\n");
  simple_bloom_filter f;
  init_simple_bloom_filter(&f, numElements * (ratio-1), bits_per_element, k);

  std::set<uint64_t> inserted_elements;

  int falsePositives = 0;
  int falseNegatives = 0;
  int total_negatives = 0;

  // get all the random elements, this uses tons of space.
  double start_insertion_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      total_negatives++;
      continue;
    }
    insert_simple_bloom_filter(&f, utils::map(i));
    inserted_elements.insert(utils::map(i));
  }
} else {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      total_negatives++;
      continue;
    }
    insert_simple_bloom_filter(&f, utils::map(i));
  }
}
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
if (!timing){
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_simple_bloom_filter(&f, utils::map(i));
    if (found && inserted_elements.find(utils::map(i)) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(utils::map(i)) != inserted_elements.end()) {
      falseNegatives++;
    }
  }
} else {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_simple_bloom_filter(&f, utils::map(i));
    if (found) {
      falsePositives++;
    }
    if (!found) {
      falseNegatives++;
    }
  }
}
  double end_lookup_time = tfk_get_time();
  /*for (int i = 0; i < numElements*2; i++) {
    bool found = lookup_simple_bloom_filter(&f, utils::hash(i));
    if (found && i % 2 == 0) {
      falsePositives++;
    }
    if (!found && i%2 != 0) {
      falseNegatives++;
    }
  }*/

if (!gen_table) {
  if (timing) {
    printf("NOTE: We are in timing mode, therefore the false positive/negatives are not accurate.\n");
  }
  printf("False positives %d, False negatives %d, total negatives %d\n", falsePositives, falseNegatives, total_negatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) total_negatives));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);
} else {
  if (!timing) {
    // Format for non-timing: numElements \t K \t False Positive Rate \t Memory
    printf("%d\t%f\t%f\t%d\n", numElements, k, 100*(float)falsePositives / ((float) total_negatives), total_allocated / 1048576);
  } else {
    // Format for timing: numElements \t K \t runtime \t Memory
    printf("%d\t%f\t%f\t%d\n", numElements, k, end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time, total_allocated / 1048576);
  }
}
/*
  float total_time = end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time;
  uint64_t total_memory = total_allocated/1048576;
  printf("%d\t%f\t%f", total_time, total_memory, 100*(float)falsePositives / ((float) numElements));
*/
  free_wrapper(f.words);
}

void sliced_bloom_filter_test (uint64_t numElements, int bits_per_element, unsigned int k, int ratio) {
  printf("Begin sliced bloom filter test\n");
  sliced_bloom_filter f;
  init_sliced_bloom_filter(&f, numElements * (ratio-1), bits_per_element, k);

  int falsePositives = 0;
  int falseNegatives = 0;


  // get all the random elements, this uses tons of space.
  double start_insertion_time = tfk_get_time();
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) continue;
    insert_sliced_bloom_filter(&f, utils::map(i));
  }
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_sliced_bloom_filter(&f, utils::map(i));
    if (found && i % ratio == 0) {
      falsePositives++;
    }
    if (!found && i%ratio != 0) {
      falseNegatives++;
    }
  }
  double end_lookup_time = tfk_get_time();
  /*for (int i = 0; i < numElements*2; i++) {
    bool found = lookup_simple_bloom_filter(&f, utils::hash(i));
    if (found && i % 2 == 0) {
      falsePositives++;
    }
    if (!found && i%2 != 0) {
      falseNegatives++;
    }
  }*/

  printf("False positives %d, False negatives %d\n", falsePositives, falseNegatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) numElements));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);

/*  float total_time = end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time;
  uint64_t total_memory = total_allocated/1048576;
  printf("%d\t%f\t%f", total_time, total_memory, 100*(float)falsePositives / ((float) numElements));
*/
  free_wrapper(f.words);
}

void scalable_bloom_filter_test (uint64_t numElements, float bits_per_element, float k, int ratio, bool timing, bool gen_table) {
if (print_human_readable){
  printf("Begin scalable bloom filter test\n");
}
  scalable_bloom_filter f;

  init_scalable_bloom_filter(&f, 512, bits_per_element + ((float)2)/0.7, k + 2);
  //init_scalable_bloom_filter(&f, 2048, bits_per_element + ((float)2)/0.7, k + 2);

  int falsePositives = 0;
  int falseNegatives = 0;
  int num_negatives = 0;
  std::set<uint64_t> inserted_elements = std::set<uint64_t>();
  // get all the random elements, this uses tons of space.
  double start_insertion_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }
    insert_scalable_bloom_filter(&f, utils::map(i));
    inserted_elements.insert(utils::map(i));
  }
} else {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }
    insert_scalable_bloom_filter(&f, utils::map(i));
  }
}
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_scalable_bloom_filter(&f, utils::map(i));
    if (found && inserted_elements.find(utils::map(i)) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(utils::map(i)) != inserted_elements.end()) {
      falseNegatives++;
    }
  }
} else {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_scalable_bloom_filter(&f, utils::map(i));
    if (found) {
      falsePositives++;
    }
    if (!found) {
      falseNegatives++;
    }
  }
}
  double end_lookup_time = tfk_get_time();
  /*for (int i = 0; i < numElements*2; i++) {
    bool found = lookup_simple_bloom_filter(&f, utils::hash(i));
    if (found && i % 2 == 0) {
      falsePositives++;
    }
    if (!found && i%2 != 0) {
      falseNegatives++;
    }
  }*/

if (!gen_table) {
  if (timing) {
    printf("NOTE: We are in timing mode, therefore the false positive/negatives are not accurate.\n");
  }
  printf("False positives %d, False negatives %d, total negatives %d\n", falsePositives, falseNegatives, num_negatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) num_negatives));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);
} else {
  if (!timing) {
    // Format for non-timing: numElements \t K \t False Positive Rate \t Memory
    printf("%d\t%f\t%f\t%llu\n", numElements, k, 100*(float)falsePositives / ((float) num_negatives), total_allocated / 1048576);
  } else {
    // Format for timing: numElements \t K \t runtime \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time, total_allocated / 1048576);
  }
}

}

void cache_efficient_bloom_filter_test (uint64_t numElements, int bits_per_element, unsigned int k, int ratio, bool timing, bool gen_table) {
  if(print_human_readable) printf("Begin cache efficient bloom filter test\n");
  cache_efficient_bloom_filter f;
  init_cache_efficient_bloom_filter(&f, numElements*(ratio-1), bits_per_element, k);

  std::set<uint64_t> inserted_elements = std::set<uint64_t>();
  int falsePositives = 0;
  int falseNegatives = 0;
  int num_negatives = 0;
  double start_insertion_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }
    insert_cache_efficient_bloom_filter(&f, utils::map(i));
    inserted_elements.insert(utils::map(i));
  }
} else {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }
    insert_cache_efficient_bloom_filter(&f, utils::map(i));
  }
}
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_cache_efficient_bloom_filter(&f, utils::map(i));
    if (found && inserted_elements.find(utils::map(i)) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(utils::map(i)) != inserted_elements.end()) {
      falseNegatives++;
    }
  }
} else {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_cache_efficient_bloom_filter(&f, utils::map(i));
    if (found) {
      falsePositives++;
    }
    if (!found) {
      falseNegatives++;
    }
  }
}
  double end_lookup_time = tfk_get_time();
if (!gen_table) {
  if (timing) {
    printf("NOTE: We are in timing mode, therefore the false positive/negatives are not accurate.\n");
  }
  printf("False positives %d, False negatives %d, total negatives %d\n", falsePositives, falseNegatives, num_negatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) num_negatives));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);
} else {
  if (!timing) {
    // Format for non-timing: numElements \t K \t False Positive Rate \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, 100*(float)falsePositives / ((float) num_negatives), total_allocated / 1048576);
  } else {
    // Format for timing: numElements \t K \t runtime \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time, total_allocated / 1048576);
  }
}

  /*float total_time = end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time;
  uint64_t total_memory = total_allocated/1048576;
  printf("%d\t%d\t%f", total_time, total_memory, 100*(float)falsePositives / ((float) numElements));
*/
  for (int i = 0; i < f.num_filters; i++) {
    free_wrapper(f.filters[i].words);
  }
  free_wrapper(f.filters);

}

void cache_efficient_scalable_bloom_filter_test (uint64_t numElements, int bits_per_element, unsigned int k, int ratio, bool timing, bool gen_table) {
  if (print_human_readable) printf("Begin cache efficient scalable bloom filter test\n");
  cache_efficient_scalable_bloom_filter f;
  float f_k = ((float)bits_per_element) * 0.7;
  init_cache_efficient_scalable_bloom_filter(&f, numElements*(ratio-1), bits_per_element, f_k);

  std::set<uint64_t> inserted_elements = std::set<uint64_t>();

  int falsePositives = 0;
  int falseNegatives = 0;
  int num_negatives = 0;
  double start_insertion_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }

    insert_cache_efficient_scalable_bloom_filter(&f, utils::map(i));
    inserted_elements.insert(utils::map(i));
  }
} else {
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) {
      num_negatives++;
      continue;
    }

    insert_cache_efficient_scalable_bloom_filter(&f, utils::map(i));
  }
}
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_cache_efficient_scalable_bloom_filter(&f, utils::map(i));
    if (found && inserted_elements.find(utils::map(i)) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(utils::map(i)) != inserted_elements.end()) {
      falseNegatives++;
    }
  }
} else {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found = lookup_cache_efficient_scalable_bloom_filter(&f, utils::map(i));
    if (found) {
      falsePositives++;
    }
    if (!found) {
      falseNegatives++;
    }
  }
}
  double end_lookup_time = tfk_get_time();
if (!gen_table) {
  if (timing) {
    printf("NOTE: We are in timing mode, therefore the false positive/negatives are not accurate.\n");
  }
  printf("False positives %d, False negatives %d, total negatives %d\n", falsePositives, falseNegatives, num_negatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) num_negatives));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);
} else {
  if (!timing) {
    // Format for non-timing: numElements \t K \t False Positive Rate \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, 100*(float)falsePositives / ((float) num_negatives), total_allocated / 1048576);
  } else {
    // Format for timing: numElements \t K \t runtime \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time, total_allocated / 1048576);
  }
}


 // float total_time = end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time;
 // uint64_t total_memory = total_allocated/1048576;
 // printf("%d\t%d\t%f", total_time, total_memory, 100*(float)falsePositives / ((float) numElements));

/*  for (int i = 0; i < f.num_filters; i++) {
    free_wrapper(f.filters[i].words);
  }
  free_wrapper(f.filters);
*/
}


void cache_efficient_bloom_filter_balanced_test (uint64_t numElements, float bits_per_element, float k, int ratio, bool timing, bool gen_table) {
  //printf("Begin cache efficient bloom filter balanced test\n");
  cache_efficient_bloom_filter f;
  init_cache_efficient_bloom_filter(&f, numElements*(ratio-1), bits_per_element*1.01, k*1.01);

  int falsePositives = 0;
  int falseNegatives = 0;
  int num_negatives = 0;
  std::set<uint64_t> inserted_elements = std::set<uint64_t>();

  double start_insertion_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements; i++) {
    inserted_elements.insert(i);
    insert_cache_efficient_bloom_filter_explicit(&f, i, i % f.num_filters);
  }
} else {
  cilk_for (int i = 0; i < numElements; i++) {
    insert_cache_efficient_bloom_filter_explicit(&f, utils::map(i), i % f.num_filters);
  }
}
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
if (!timing) {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found;
    if (i < numElements) {
      found = lookup_cache_efficient_bloom_filter_explicit(&f, i, i % f.num_filters);
    } else {
      num_negatives++;
      found = lookup_cache_efficient_bloom_filter_explicit(&f, i, i % f.num_filters);
    }
    if (i < numElements) {
    if (found && inserted_elements.find(i) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(i) != inserted_elements.end()) {
      falseNegatives++;
    }
    } else {
    if (found && inserted_elements.find(i) == inserted_elements.end()) {
      falsePositives++;
    }
    if (!found && inserted_elements.find(i) != inserted_elements.end()) {
      falseNegatives++;
    }

    }
  }
} else {
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    bool found;
    if (i % ratio == 0) {
      // For elements which were not inserted, perform a lookup in a random bucket.
      // Using i % f.num_filters for these elements is unfair and causes the false
      //  positive rate to be artifically low.
      found = lookup_cache_efficient_bloom_filter(&f, utils::map(i)); // try to avoid collisions
    } else {
      found = lookup_cache_efficient_bloom_filter_explicit(&f, utils::map(i), i % f.num_filters);
    }
    if (found && i % ratio == 0) {
      falsePositives++;
    }
    if (!found && i%ratio != 0) {
      falseNegatives++;
    }
  }

}
  double end_lookup_time = tfk_get_time();
if (!gen_table) {
  if (timing) {
    printf("NOTE: We are in timing mode, therefore the false positive/negatives are not accurate.\n");
  }
  printf("False positives %d, False negatives %d, total negatives %d\n", falsePositives, falseNegatives, num_negatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) num_negatives));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  printf("Total time %f\n", end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time);
} else {
  if (!timing) {
    // Format for non-timing: numElements \t K \t False Positive Rate \t Memory
    printf("%d\t%f\t%f\t%llu\n", numElements, k, 100*(float)falsePositives / ((float) num_negatives), total_allocated / 1048576);
  } else {
    // Format for timing: numElements \t K \t runtime \t Memory
    printf("%d\t%d\t%f\t%d\n", numElements, k, end_insertion_time - start_insertion_time + end_lookup_time - start_lookup_time, total_allocated / 1048576);
  }
}


  for (int i = 0; i < f.num_filters; i++) {
    free_wrapper(f.filters[i].words);
  }
  free_wrapper(f.filters);

}

void cache_efficient_bloom_filter_sizes_test (uint64_t numElements, int bits_per_element, unsigned int k, int ratio) {
  //printf("Begin cache efficient bloom filter sizes test\n");
  bits_per_element = 1*bits_per_element;

  // precompute the sizes.
  int cache_lines = 2;
  int num_filters = bits_per_element*numElements/(cache_lines*64*8);
  int* sizes = (int*) calloc(sizeof(int), num_filters);
  for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) continue;
    uint64_t element = utils::map(i);
    uint64_t filter = utils::shard(element) % num_filters;
    sizes[filter]++;
  }
  int max_size = -1;
  int min_size = numElements;
  int total_size = 0;
  for (int i = 0; i < num_filters; i++) {
    if (sizes[i] > max_size) max_size = sizes[i];
    if (sizes[i] < min_size) min_size = sizes[i];

    total_size += sizes[i];
  }
  printf("max size is %d, min size is %d , avg size: %d, num_filters:%d\n", max_size, min_size,
      total_size/num_filters, num_filters);
  // init using the sizes.
  cache_efficient_bloom_filter f;
  init_cache_efficient_bloom_filter_with_sizes(&f, numElements*(ratio-1), 2*bits_per_element, 2*k, num_filters, sizes);

  int falsePositives = 0;
  int falseNegatives = 0;

  double start_insertion_time = tfk_get_time();
  cilk_for (int i = 0; i < numElements * ratio; i++) {
    if (i % ratio == 0) continue;
    uint64_t element = utils::map(i);
    insert_cache_efficient_bloom_filter(&f, element);
  }
  double end_insertion_time = tfk_get_time();

  double start_lookup_time = tfk_get_time();
  cilk_for (int i = 0; i < numElements*ratio; i++) {
    uint64_t element = utils::map(i);
    bool found = lookup_cache_efficient_bloom_filter(&f, element);
    if (found && i % ratio == 0) {
      falsePositives++;
    }
    if (!found && i%ratio != 0) {
      falseNegatives++;
    }
  }
  double end_lookup_time = tfk_get_time();

  printf("False positives %d, False negatives %d\n", falsePositives, falseNegatives);
  printf("False positive rate %f\n", 100*(float)falsePositives / ((float) numElements));
  printf("Insertion time %f\n", end_insertion_time - start_insertion_time);
  printf("Lookup time %f\n", end_lookup_time - start_lookup_time);
  free_wrapper(sizes);
  for (int i = 0; i < f.num_filters; i++) {
    free_wrapper(f.filters[i].words);
  }
  free_wrapper(f.filters);
}


int main(int argc, char **argv)
{
  if (argc < 4) {
    printf("Error not enough args. Usage: ./main \n");
  }
  int argument = atoi(argv[1]);
  uint64_t N = argument << 18;

  float bits_per_element = atoi(argv[2])/0.7;

  int algorithm = atoi(argv[3]);

  //uint64_t N = 1 << 27;
  //int bits_per_element = 20;
  float k = bits_per_element*0.70;
  if (print_human_readable) printf("k=%f, c=%f\n", k, bits_per_element);
  int ratio = 2;

  // give a random seed.
  uint32_t srand_seed = tfk_get_time();
  //printf("using srand_seed: %d\n", srand_seed);
  srand(srand_seed);
  utils::seed1 = rand();
  utils::seed2 = rand();
  utils::seed_map = rand();
  utils::seed_shard = rand();
  total_allocated = 0;
bool runtime = false;
bool table = false;
if (algorithm == -1 || algorithm == 0) {
  simple_bloom_filter_test(N, bits_per_element, k, ratio, runtime, table);
  if (print_human_readable) printf("total allocated memory %llu MB\n\n\n", total_allocated / 1048576);
  total_allocated = 0;
}

if (algorithm == -1 || algorithm == 1) {
  cache_efficient_bloom_filter_test(N, bits_per_element, k, ratio, runtime, table);
  if (print_human_readable) printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
  total_allocated = 0;
}

//  printf("\t");
  //cache_efficient_bloom_filter_balanced_test(N, bits_per_element, k, ratio);
  //printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);

if (algorithm == -1 || algorithm == 2) {
  SCALABLE_FILTER_RESIZE_RATE = 0.2;
  FIXED_RESIZE = true;
  total_allocated = 0;
  cache_efficient_scalable_bloom_filter_test(N, bits_per_element, k, ratio, runtime, table);
  if (print_human_readable) printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
}

if (algorithm == -1 || algorithm == 3) {
  SCALABLE_FILTER_RESIZE_RATE = 2;
  total_allocated = 0;
  scalable_bloom_filter_test(N, bits_per_element, k, ratio, runtime, table);
  if (print_human_readable) printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
}


if (algorithm == -1 || algorithm == 4) {
  total_allocated = 0;
  cache_efficient_bloom_filter_balanced_test(N, bits_per_element, k, ratio, runtime, table);
  if (print_human_readable) printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
}

//  total_allocated = 0;
//  scalable_bloom_filter_test(N, bits_per_element, k, ratio);
//  printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
//  total_allocated = 0;
//  sliced_bloom_filter_test(N, bits_per_element, k, ratio);
//  printf("total allocated memory %llu MB\n\n\n", total_allocated/1048576);
//  printf("total needed to resize %d out of %d\n", total_needed_to_resize, N);
//  total_allocated = 0;
//  scalable_bloom_filter_test(N, bits_per_element, k, ratio);
  return 0;
}

