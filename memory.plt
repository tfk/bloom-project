set title "Inserted elements versus memory"
set xlabel "Number of inserted elements"
set ylabel "Sum of Block Sizes (MB)"
plot "table.tsv" using 1:2 with linespoints title "Standard BF Memory", "table.tsv" using 1:5 with linespoints title "Blocked BF Memory", "table.tsv" using 1:8 with linespoints title "Dynamic BF Memory";
set terminal unknown
