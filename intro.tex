\secput{intro}{Introduction}

Bloom filters are an efficient data structure for performing approximate set
membership queries. A traditional bloom filter maintains an array of $m$ bits
to represent a set of size $n$. It utilizes $k$ hash functions to map each
element in some universe to $k$ random bits in its array. An element is
inserted by setting its $k$ associated bits to \const{true}. A query for approximate
set membership is performed for an element by taking the bitwise AND of its $k$
bits. If the element was inserted into the bloom filter, a lookup for that
element is guaranteed to be correct.  Sometimes, however, an element which is
not in the set will be mapped to $k$ bits which have been set due to the
insertion of other elements. In this case, a \defn{false positive} is reported
for that element.

One problem with traditional bloom filters is that they require $k$ random IO
operations to perform insertions and approximate membership queries. If a bloom
filter is stored on disk, it may be necessary to perform $k$ disk seeks to
access $k$ random bits in the filter's array. These random IO operations can
also be expensive when the bloom filter fits into main memory. 

A modern shared memory machine has a heirarchy of caches. Typically there are
four caches of increasing size: L1, L2, and L3. The cost of accessing each
level of the cache grows dramatically. On the modern multicore machine used for
the experiments in this paper the L1-cache is 32 KB, the L2-cache is 128 KB,
and the L3-cache is 12 MB. Even moderately sized bloom filters may exceed 12 MB
in space. Accessing $k$ random bits in a bit array much larger than 12 MB may
result in up to $k$ L3-cache misses.

This paper explores methods for modifying the standard bloom filter to improve
the memory locality of insertions and approximate membership queries. Ideally,
each element would be mapped to $k$ bits located on the same cache line
(usually 64 bytes) or on the same memory page (usually 4096 bytes). Even coarser
memory locality, however, may still help reduce the number of L3-cache misses and
improve performance. 

\defn{Blocked bloom filters}, developed by Putze, Sanders, and Singler, provide
a potential solution to the poor memory locality of standard bloom filters
\cite{PutzeFe10}. A blocked bloom filter is composed of $b$ smaller standard
bloom filters of equal size. Each element is mapped to $k$ bits within a single
randomly assigned bloom filter. The problem with this approach, however, is
that the random assignment of inserted elements to blocks will result in some
blocks becoming overloaded. Inserting too many elements into a given block will
increase its false probability rate. To compensate, \cite{PutzeFe10} proposes
using more space by scaling the size of each block by approximately $15$ to
$30\%$.

This paper considers the question: can we efficiently resize each of the $b$
bloom filters dynamically according to its load to save space? The notion of
bloom filters which can resize dynamically has been explored previously.
\defn{Scalable bloom filters} developed by Baquero, Hutchison, and Preguica
maintain a predetermined failure probability even when the number of inserted
elements is not known in advance \cite{AlmeidaBa07}. This data structure
may be used to implement a \defn{dynamic blocked bloom filter} whose composite bloom
filters resize dynamically in response to unequal load. This data structure
is fairly natural since it is merely a composition of existing work. To the
best of the author's knowledge, however, this data structure has not been
analyzed in previous work.

Four types of bloom filters have been implemented during the course of this
project. The standard bloom filter, the blocked bloom filter, the dynamic
(scalable) bloom filter, and the dynamic blocked bloom filter.  The first
contribution of this paper is a survey of the three bloom filter variants
previously described in the literature coupled with some empirical analysis of
their performance when implemented. The second contribution is the design and
empirical analysis of the dynamic blocked bloom filter which will take
advantage of the machinery developed during the course of our survey.

The following is a brief outline of the contents of the paper.

\begin{closeitemize}
\item (\secref{standardbloom}) Design and implementation of the standard bloom
filter data structure which requires only $2$ independent hash functions,
independent of $k$.

\item (\secref{blockedbloom}) A blocked bloom filter which is analyzed and
empirically tested in two cases: the case in which insertions are equally
distributed amongst all blocks, and the case in which insertions are assigned
to blocks according to a random hash function.

\item (\secref{scalablebloom}) A scalable bloom filter which can resize itself
dynamically at runtime while maintaining the same false positive rate is
described and analyzed. Empirical results provide a direct comparison of the
false positive rate and memory usage of scalable bloom filters and standard
bloom filters. Such a direct comparison appears to have been absent from the
original scalable bloom filter paper.
 
\item (\secref{scalableblockedbloom}) A dynamic blocked bloom filter is
described and analyzed. The false positive rate, runtime, and memory usage is
compared with the standard bloom filter and the blocked bloom filter.

\item (\secref{conclusion}) A brief conclusion which includes a few informal
remarks on some related ideas I found interesting, but did not have time to
explore in this paper.

\end{closeitemize}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "proposal"
%%% End: 

% LocalWords:  PageRank PageRanks vertices GraphLab parallelize parallelizing
% LocalWords:  nondeterministic nondeterminism runtime parallelized Seidel MIS
% LocalWords:  nonatomic parallelizes superlinear Luby's fluidanimate Plassmann
% LocalWords:  determinize multicore

