LATEX=latex
DOC=bloom
BIB=allpapers
SECTIONS= abstract \
	intro \
	standardbloom \
	scalablebloom \
	blockedbloom \
	scalableblockedbloom \
	conclusion \

# This line is intentionally blank.

SECTIONS_TEX = $(patsubst %,%.tex,$(SECTIONS))
EPS_FIGNAMES = 

EPS_FILES = $(patsubst %,figs/%.eps,$(EPS_FIGNAMES))
FIG_MAG_cachemods=-m 0.5
FIG_MAG_snapshot=-m 0.5

OTHERS = Makefile supertech.sty clrscode4e.sty

default: pdf
$(DOC)-flattened.tex: $(DOC).tex
	cd flatten;make
	flatten/flatten $< $@
.PRECIOUS: %.dvi
.PHONY: default all fast dvi ps pdf print view clean bbl
fast all: $(DOC).ps $(DOC).dvi
dvi: $(DOC).dvi 
ps: $(DOC).ps
pdf: $(DOC).pdf
print: $(DOC).ps
	lpr $(DOC).ps
view: $(DOC).ps
	gv -resize $(DOC).ps &
bbl:
	if -e $(DOC).bbl; then rm $(DOC).bbl; fi
	latex $(DOC)
	bibtex --min-crossrefs=9999 $(DOC)

$(DOC).bbl: $(BIB).bib $(DOC).tex $(SECTIONS_TEX) $(OTHERS)
	latex $(DOC)
	bibtex --min-crossrefs=9999 $(DOC)

$(DOC).dvi: $(DOC).tex $(SECTIONS_TEX) $(FIG_PSTEX_TS)  $(FIG_PSTEXS) $(EPS_FILES) $(DOC).bbl $(OTHERS)
$(DOC).ps:  $(DOC).dvi $(FIG_PSTEXS)

x:
	xdvi $(DOC)

# This is less annoying than the run_latex script, since it doesn't print so much and it runs faster.
# Algorithm:  run latex, but don't print the output to the terminal.
# If you see the word "Citation" in the log, the run bibtex and run latex again (don't print latex log out yet)
# If you see "Label(s) may have changed" then run latex a third time (still don't print the the latex log)
# Finally, print the latex log
# When running bibtex, be sure to use --min-crossrefs=9999
%.dvi: %.tex
	@echo latex $(patsubst %.tex,%,$<)
	@latex $(patsubst %.tex,%,$<)
	@if fgrep -e "Citation" $(patsubst %.tex,%.log,$<) | fgrep -v "???" >/dev/null; then echo bibtexing and latexing again because of an undefined citation; echo bibtex --min-crossrefs=9999 $(patsubst %.tex,%,$<); bibtex --min-crossrefs=9999 $(patsubst %.tex,%,$<); echo latex $(patsubst %.tex,%,$<) ; latex $(patsubst %.tex,%,$<) ; fi
	@if fgrep -e "Label(s) may have changed" $(patsubst %.tex,%.log,$<) > /dev/null; then echo Latexing again because labels changed.; echo latex $(patsubst %.tex,%,$<); latex $(patsubst %.tex,%,$<); fi
%.pstex: %.fig Makefile
	fig2dev -L pstex $($(patsubst %.fig,FIG_MAG_%,$<)) $< $@
%.pstex_t: %.fig Makefile
	fig2dev -L pstex_t -p $(patsubst %.fig,%.pstex,$<) $($(patsubst %.fig,FIG_MAG_%,$<)) $< $@
# nsf says use -P pdf -t letter to avoid using type 3 fonts. 
# However there is a problem with the fl ligature, and I cannot fix it byu going the other route
# Rehdat bugzilla Bug 66721 reports that passing -G0 to dvips will fix this problem.
DVIPS = dvips -P pdf -t letter -G0
#DVIPS = dvips
%.ps: %.dvi
	$(DVIPS) $(patsubst %.ps,%,$@) -o

%.eps: %.gnuplot
	gnuplot $<

# Method for creating .eps figures from PowerPoint source
#  - Print from PowerPoint to PostScript printer
#  - Set "Print to file" and "Current slide"
#  - Transfer resulting .prn file to Linux
#  - Run script below to create .eps

%.eps:	%.prn Makefile
	fixps $< -o $(patsubst %.prn, %.ps, $<)
	ps2epsi $(patsubst %.prn, %.ps, $<) $@

%.eps: %.fig Makefile
	fig2dev -L eps $< $@

%.pdf: %.ps
	ps2pdf $< $@

# tm.bbl: tm.bib $(DOC).tex deliverables.tex milestones.tex technical.tex latex8.bst
#	bibtex --min-crossrefs=9999 $(DOC)

publish: $(DOC).pdf $(DOC).ps
	cat $(DOC).tex $(SECTIONS_TEX) >spaa048-leiserson.tex
	cp $(DOC).ps spaa048-leiserson.ps
	@echo "Create spaa048-leiserson.pdf using Adobe Distiller with ACM parameters."


ChangeLog:
	$(RM) $@
	rcs2log -h csail.mit.edu -v > $@
.PHONY: ChangeLog

clean:
	rm -f *.aux *.ps *.pdf *.dvi *.bbl *.blg *.tmp *.log *.pstex *.pstex_t *~




