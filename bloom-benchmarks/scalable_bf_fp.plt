set title "Scalable Bloom Filter False Positive Rate"
set xlabel "Number of Hash Functions"
set ylabel "False Positive Rate (Percentage)"
set log x 2
set log y 2
set xlabel font ",14"
set ylabel font ",14"
set key font ",12"
set xtics font ",11"
set ytics font ",11"
set title font ",16"
plot "scalability_test_alg0.tsv" using 2:3 with linespoints title "Standard BF", "scalability_test_alg3.tsv" using 2:3 with linespoints title "Scalable BF (initial size 2048)", "scalability_test_alg3_512.tsv" using 2:3 with linespoints title "Scalable BF (initial size 512)";
