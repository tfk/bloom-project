set title "Dynamic Blocked Bloom Filter Runtime"
set xlabel "Number of Inserted Elements"
set ylabel "Speedup Relative to Standard BF"
set xlabel font ",14"
set ylabel font ",14"
set key font ",12"
set log x 2
set xtics font ",9"
set ytics font ",11"
set yrange[:1.2]
set title font ",16"
plot "runtime_alg0_relative.tsv" using 1:3 with lines title "Standard BF", "runtime_alg1_relative.tsv" using 1:3 with linespoints title "Blocked BF", "runtime_alg2_relative.tsv" using 1:3 with linespoints title "Dynamic Blocked BF";
