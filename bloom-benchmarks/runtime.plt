set title "Dynamic Blocked Bloom Filter Runtime"
set xlabel "Number of Inserted Elements"
set ylabel "Runtime (seconds)"
set xlabel font ",14"
set ylabel font ",14"
set key font ",12"
set yrange[:525]
set xrange[:280000000]
set xtics font ",11"
set ytics font ",11"
set title font ",16"
plot "runtime_alg0.tsv" using 1:3 with linespoints title "Standard BF", "runtime_alg1.tsv" using 1:3 with linespoints title "Blocked BF", "runtime_alg2.tsv" using 1:3 with linespoints title "Dynamic Blocked BF";
