set title "Inserted elements versus FP Rate"
set xlabel "Number of inserted elements"
set ylabel "FP Rate (Percent)"
plot "table.tsv" using 1:4 with linespoints title "Standard BF FP", "table.tsv" using 1:7 with linespoints title "Blocked BF FP", "table.tsv" using 1:10 with linespoints title "Dynamic BF FP";
set terminal unknown
