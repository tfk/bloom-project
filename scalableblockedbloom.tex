\secput{scalableblockedbloom}{Dynamic Blocked Bloom Filter}

The blocked bloom filter previously described was composed of $b$ standard
bloom filters whose sizes were determined statically based on their expected
sizes. The \defn{dynamic blocked bloom filter} is a blocked bloom filter in
which the size of each block is grows dynamically as the number of elements
inserted increases. This allows the dynamic blocked bloom filter to maintain a
low failure probability without needing to scale the size of the entire table
by a fixed percentage. In practice, the failure probabilities provided by the
dynamic blocked bloom filter match those of the standard bloom filter fairly
closely for values of $k$ between $1$ and $15$. \fillintheblank{More bottom
line results.}

\subheading{Implementation}

The dynamic blocked bloom filter is composed of $b$ blocks. Each block is a
scalable bloom filter which is configured with tightening ratio $r = 1/40$.
Suppose we want a failure probability $P$. A standard bloom filer achieves that
failure probability with $k = \lg (1/P)$. If the scalable bloom filter starts
with failure probability $P_{0}$, then its failure probability will be bounded
by $P_{0}/(1-r) = 40P_{0}/39$.  This implies that we want to choose $P_{0} =
39P/40$ to guarantee a failure probability of $P$. $k' = \lg (40/39P) = \lg
(40/39) + k$.  This small increase in $k$ is unlikely to change its integral
value, however, it does require us to adjust the average number of bits stored
per element.  This compensation requires us to increase our space usage by
approximately $5\%$ because $\lg (40/39) / 0.7 \approx 0.052$. 

The scalable blocked bloom filter is simply a blocked bloom filter in which
each block is a scalable bloom filter. In this implementation, the initial size
of each block is set to its size in the ideal case in which all inserted
elements are sharded evenly. When a block is overloaded, it resizes itself to
maintain a bound on its failure probability.


\subheading{Performance results}

\begin{figure}
\begin{minipage}[ht]{0.60\textwidth}
\vspace{1ex}
  \footnotesize
 \includegraphics[scale=0.4]{figs/dynamic_blocked_bloom_filter_runtime_numelements.eps}
 \includegraphics[scale=0.4]{figs/dynamic_blocked_bloom_filter_runtime_mem.eps}
\end{minipage}
\hspace{40pt}\begin{minipage}[ht]{0.40\textwidth}
  \caption{Two plots showing the speedup achieved by the static and dynamic
blocked bloom filter when compared to the standard bloom filter. The x-axis of
the first plot shows the number of elements inserted. The x-axis of the second
plot shows the total space in MB used by the filter. These benchmarks were run
on a single core of a multicore machine. The machine used was an Intel Xeon
X5650 with 49GB DRAM, two 12-MB L3-caches each shared between 6 cores, and
private L2- and L1-caches with 128 KB and 32 KB, respectively. Since the
benchmark was run on a single core the program had access to 4 GB of DRAM, a
single 12-MB L3-cache, a 128 KB L2-cache, and a 32 KB L1-cache.}

  \label{fig:scale-memory} \end{minipage}
\end{figure}

\begin{figure}
\begin{minipage}[ht]{1.0\textwidth}
\vspace{1ex}
  \footnotesize
\end{minipage}
\begin{minipage}[ht]{1.0\textwidth}
  \caption{A plot comparing the number of inserted elements versus the total
  runtime (including insertions and lookups).}
  \label{fig:scale-runtime} \end{minipage}
\end{figure}

\begin{figure}
\centering
\begin{minipage}[ht]{1.0\textwidth}
\vspace{1ex}
  \footnotesize
\centering
 \includegraphics[scale=0.5]{figs/dynamic_blocked_bloom_filter_fp_rate.eps}
\end{minipage}
\begin{minipage}[ht]{1.0\textwidth}
  \caption{Plot of the false positive rate of the dynamic blocked bloom filter,
the blocked bloom filter and the standard bloom filter as a function of the
number of hash functions used. The x-axis is on a log (base 2) scale. The false
positive rate was calculated by inserting $2^{24}$ random elements into each
set and then performing $2^{25}$ lookups. Half of the lookups were guaranteed
to be positive. False positives were detected using an exact set data structure
containing all inserted elements.}

  \label{fig:scale-fp} \end{minipage}
\end{figure}


The scalable blocked bloom filter was compared against the standard bloom
filter and the blocked bloom filter. \figref{scale-memory} demonstrates that
the scalable blocked bloom filter uses more memory than  the standard bloom
filter, but less than the blocked bloom filter. \figref{scale-runtime}
demonstrates that the scalable bloom filter runs nearly as fast as the blocked
bloom filter which is itself significantly faster than the standard bloom
filter. Finally, \figref{scale-fp} shows how the failure probability varies
between these three approaches.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "proposal"
%%% End: 

% LocalWords:  PageRank PageRanks vertices GraphLab parallelize parallelizing
% LocalWords:  nondeterministic nondeterminism runtime parallelized Seidel MIS
% LocalWords:  nonatomic parallelizes superlinear Luby's fluidanimate Plassmann
% LocalWords:  determinize multicore

