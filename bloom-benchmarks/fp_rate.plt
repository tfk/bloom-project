set title "False Positive Rate of Dynamic Blocked BF"
set xlabel "Number of Hash Functions"
set ylabel "FP Rate (Percent)"
set log x 2
set log y 2
set xlabel font ",14"
set ylabel font ",14"
set key font ",12"
set xtics font ",11"
set ytics font ",11"
set title font ",16"

plot "table0_nontiming.tsv" using 2:3 with linespoints title "Standard BF FP", "table1_nontiming.tsv" using 2:3 with linespoints title "Blocked BF FP", "table2_nontiming.tsv" using 2:3 with linespoints title "Dynamic Blocked BF FP";
set terminal unknown
