\secput{blockedbloom}{Blocked Bloom Filters}

In this section we describe the blocked bloom filter as described in
\cite{PutzeFe10} and analyze its real world performance when its blocks are
evenly and unevenly loaded.

\subheading{Definitions}

Suppose we have a blocked bloom filter $X_{B}(n,c,k,b)$ where $b$ is the number
of blocks. Let $n$ be the total number of elements inserted into $X$. Let
$n_{B}$ be the number of elements which were inserted into block $B$ so that
$\sum_{i=1}^{B} n_{B} = n$.  As with the standard bloom filter, we will store
$c$ bits for each element $n$ so that the combined size of all $B$ bloom
filters is $cn$ bits.  After all elements have been inserted, each of the $B$
bloom filters has some false positive probability $f_{B}$. 

Now consider an element $e$ which was not inserted into $X_{B}$. To lookup $e$
we map $e$ to a block $B$. We then perform a regular bloom filter lookup for
$e$ in block $B$. The false positive probability in block $B$ is $f_{B}$.
Therefore, we have that $Pr(\textit{false positive on $e$} | e \rightarrow B) =
f_{B}$. Using the chain rule of probability we can compute the probability of a
false positive when looking up $e$ in $X$.

\begin{equation*}
Pr(\textit{false positive on $e$}) = \sum_{B}\frac{f_{B}}{b}
\end{equation*}

\subheading{Implementation with equally sized blocks}

Our implementation of this blocked bloom filter $X_{B}(n,c,k,b)$ attempts to
place each of its blocks on $L$ $64$-byte cache lines by setting the number of
blocks $b$ to $1+cn/(64 \cdot 8 \cdot L)$ and the block size to $64 \cdot 8
\cdot L / c$. Each block is initilized as a standard bloom filter 
$X_{S}(64 \cdot 8 \cdot L / c, c, k)$. 

To perform insertions and lookups a hash function $s: U \rightarrow
\{1,\ldots,b\}$ is used to shard elements from the universe $U$ into blocks of
$X_{B}$. Our implementation utilizes the MurmurHash function (with a distinct
seed) to perform this sharding.

\subheading{Experiment: Perfectly balanced case}

The blocked bloom filter implementation with evenly sized blocks will perform
suboptimally if elements are sharded to blocks unevenly. The best case
performance of the blocked bloom filter, therefore, can be ascertained
by providing a distribution of insertions which shard evenly into the
blocks of $X_{B}$. 

First we analyze the blocked bloom filter in the ideal case in which $n/B$
elements are inserted into each block. Let the size of each bloom filter $m_{B}
= cn/B$. In this case, we have that $f_{B} = (1-e^{-k/c})^{k}$. For a given
element $e$ the false positive probability is

\begin{align*}
Pr(\textit{false positive on $e$}) &= \sum_{i=1}^{B}\frac{f_{B}}{B}\\
&= (1-e^{-k/c})^{k}\\
\end{align*}

Indeed, we observe that in practice blocked bloom filters have false positive
probabilities that are asymptotically the same as the standard bloom filter
with the same parameters $c,k$.

To perform this experiment the sharding function $s$ was modified so that the
$i^{th}$ inserted element was assigned the block $i$ modulo $b$. When
performing lookups, the regular sharding function is applied to all elements
which were not inserted into the set. The modified sharding function is only
used during lookups for elements that were inserted into the set. Therefore,
this experiment enforces a uniform distribution of inserted elements amongst
the $b$ blocks, but allows any distribution of lookups amongst the $b$ blocks
for the set of elements not inserted. This experiment was run with $L = 2$ with
the goal of storing each block on two cache lines.  The results of the
experiment are summarized in \figref{blocked-bf-performance}.

\begin{figure*}
\centering
\begin{minipage}[t]{1.0\textwidth}
\vspace{1ex}
\centering
  \footnotesize
  \begin{tabular}[t]{|lcccc|}
    \hline
    \textbf{Bloom Filter} &  \textbf{Cache Misses} & \textbf{Runtime} & \textbf{False Positive Rate} &\textbf{Memory} \\
    \hline
    1. Standard BF& 16.256 M/sec &  21.720s &3.04\%& 320MB\\
    2. Blocked BF Balanced& 6.735 M/sec & 8.577s &0.18\%& 320MB\\
    3. Blocked BF Unbalanced& 11.847 M/sec & 12.981s &3.22\%& 320MB\\
    4. Blocked BF Extra Space& 11.909 M/sec & 13.084s &3.066\%& 414MB\\
    %5. Blocked BF Dynamic Resizing& 16.219 M/sec & 18.438s &3.071\%& 360MB\\
     \hline
  \end{tabular}
\end{minipage}

\begin{minipage}[t]{1.1\textwidth}
  \caption{ Comparison of the cache performance and runtime of the several
variants of the standard bloom filter and the blocked bloom filter under
different sharding distributions. The cache misses statistic was gathered using
the \textit{perf stat} command. These experiments were run using parameters
$n = 2^{27}$ elements, $c = 20$, $k = 14$. $n$ insertions were performed 
followed by $2n$ lookups (half of which were contained in the set).} 

\label{fig:blocked-bf-performance}
\end{minipage} \end{figure*}



%\begin{verbatim}
%// Perf stat output for standard bloom filter
% Performance counter stats for './main':
%
%  261791.389684  task-clock-msecs         #     11.904 CPUs 
%           1890  context-switches         #      0.000 M/sec
%             29  CPU-migrations           #      0.000 M/sec
%          82630  page-faults              #      0.000 M/sec
%   697755232656  cycles                   #   2665.310 M/sec
%    89419700006  instructions             #      0.128 IPC  
%     7877231618  cache-references         #     30.090 M/sec
%     4209932692  cache-misses             #     16.081 M/sec
%
%   21.992613017  seconds time elapsed
%
%// Perf stat output for blocked bloom filter with balanced blocks
% Performance counter stats for './main':
%
%  185064.771665  task-clock-msecs         #     11.873 CPUs 
%           1537  context-switches         #      0.000 M/sec
%             19  CPU-migrations           #      0.000 M/sec
%          85181  page-faults              #      0.000 M/sec
%   493238863986  cycles                   #   2665.223 M/sec
%   102643449390  instructions             #      0.208 IPC  
%     3864403496  cache-references         #     20.881 M/sec
%     2168787267  cache-misses             #     11.719 M/sec
%
%   15.587165049  seconds time elapsed
%\end{verbatim}


%TODO(TFK): Insert data for balanced bloom filter.

\subheading{Unbalanced case}

If each of the $n$ inserted elements are added a random block, then $n_{B}$
will vary between blocks according to a binomial distribution. For example,
when $2^{25}$ elements are inserted into $655360$ bins the average is $51$ but
the minimum is $20$ and the maximum is $90$. This imbalance translates into a
larger false positive probability. For example, when $n=2^{25}$, $c=10$, $k=7$
the standard bloom filter has a false positive probability of $1.5$ percent,
but the unbalanced blocked bloom filter had a false failure probability of $2$
percent.

Previous work on blocked bloom filters has noted that for small values of $k$
the effect of this imbalance on the false positive rate can be mitigated by
increasing the parameter $c$, the number of bits per inserted element. Indeed,
by increasing $c$ by $30\%$ the blocked bloom filter achieves a false positive
rate of $1.46\%$ on the same benchmark. 

The bloom filter which uses extra space to compensate for the unbalanced
distribution of elements into its blocks is referred to as Blocked BF Extra
Space in row $4$ of \figref{blocked-bf-performance}. The unbalanced blocked
bloom filter which does not use extra space is referred to in row $3$ as Blocked
BF Unbalanced. In the results reported in
the figure, the $30\%$ extra space allows for a lower false positive rate than
in row $3$. However, its false positive rate is still higher than the standard
bloom filter.

The remainder of this paper will discuss how ``scalable bloom filters'' can be used
to allow each block of $X_{B}$ to grow dynamically when it is overloaded.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "proposal"
%%% End: 

% LocalWords:  PageRank PageRanks vertices GraphLab parallelize parallelizing
% LocalWords:  nondeterministic nondeterminism runtime parallelized Seidel MIS
% LocalWords:  nonatomic parallelizes superlinear Luby's fluidanimate Plassmann
% LocalWords:  determinize multicore

