\secput{standardbloom}{Standard Bloom Filters}

In this section we review the basic theoretical properties of the standard
bloom filter for approximate membership queries. We then briefly describe our
implementation of the standard bloom filter specifying how to reduce the cost
of computing $k$ hash functions and how to guarantee correctness when elements
are inserted into the bloom filter concurrently.

\subheading{Formal description and analysis}

Let $S$ be a set of elements that is a subset of some universe $U$. Let
$X_{S}(n,c,k)$ be a \defn{standard bloom filter} utilizing $cn$ bits to perform
approximate membership queries on a set of size $cn$. Each element $e \in U$
can be mapped to $k$ random bits of $X_{S}$ using $k$ independent hash
functions $h_{1},\ldots, h_{k}$. To insert an element $e$ into $X_{S}$ the bits
$h_{1}(e),\ldots, h_{k}(e)$ are all set to \const{true}. To perform an approximate
membership query for the element $e$ the bitwise AND of its $k$ bits in $X_{S}$
is computed.

The \defn{false positive rate} for $X_{S}$, $f_{S}$, is the probability that
$X_{S}$ reports that an element $e \notin S$ is a member of the set. The
parameter $k$ can be optimized to minimize the false positive rate as a
function of $c$. After inserting $n$ elements, the probability that a given bit
is empty is $(1-1/cn)^{nk}$. This can be rewritten as $((1-1/cn)^{cn})^{k/c}
\approx e^{-k/c}$. The probability of reporting a false positive for an element
$e \notin S$ is equal to the probability of the $k$ random bits assigned to $e$
being set in $X_{S}$. This probability is the product $f_{S} \approx
(1-e^{-k/c})^{k}$. The approximation for $f_{S}$ is minimized when $k = c\ln 2
\approx 0.7c$. 

We note that for this optimal value of $k$ the probability that any given bit
will be \const{true} is $(1 - e^{-k/c}) \approx 1/2$ once all elements have
been inserted. Therefore, for the optimal values of $k$ and $c$ the false
positive rate for $X_{S}$ will be $f_{S} \approx 1/2^{k}$.

\subheading{Hash functions}

The primary implementation concern with the standard bloom filter is the choice
of hash functions. If the $k$ bit indicies for a given element
$h_{1}(e),\ldots, h_{k}(e)$ are expensive to compute then they may dominate the
cost of insertions and lookups. The hash functions must have certain
properties, however, or else the false failure rate may increase unacceptably.
For example, if the $k$ hash functions are only pairwise independent then it
has been shown that the false positive rate can increase by a constant factor
\cite{KirschAd08}.

It turns out, however, that it is possible to use $2$ independent hash
functions to simulate $h_{1},\ldots, h_{k}$ while maintaining the same
asymptotic false probability rate as when the $k$ hash functions are
independent.  This result is presented by Adam Kirch, and Michael Mitzenmachert
in the paper ``Less Hashing, Same Performance: Building a Better Bloom Filter''
\cite{KirschAd08}. Their scheme utilizes two hash functions $H_{1}, H_{2}$, and
uses the formula $h_{i} = H_{1} + i H_{2}$. 

Using this technique reduces the problem of computing $k$ random bit indices to
that of computing $2$ independent hash functions. Our implementation computes
the two necessary hash functions $H_{1},H_{2}$ using the MurmurHash
\cite{Appleby11} function which provides good performance in practice. We seed
each of the two hash functions by utilizing the C library's standard random
number generator seeded with the current time. 

\subheading{Concurrent access}

This paper will not focus on the concurrent performance of bloom filter
variants. It is worth noting, however, that concurrent insertions may
introduce the possibility of false negatives in bloom filters.
The complexity in implementing a standard bloom filter allowing
concurrent inserts arises because most architectures do not support atomic
writes to a single bit.  Commonly, hardware only guarantees that reads and
writes to whole bytes will appear atomic. As elements are being inserted into
the bloom filter, multiple elements may attempt to set a bit in the same byte.
Suppose two processors attempt to set two different bits to $1$ in the same
byte $b$. Each processor will read the byte $b$ into memory, set the
appropriate bit, and then store its modified copy of $b$. If both processors
read the byte $b$ before either modifies it, then the first write will be
overridden by the second.

In practice, this causes the standard bloom filter to have a non-zero false
negative probability! For example $X_{S}(n=2^{27}, c=20, k=14)$ reports a
number of false negatives commonly ranging from $0$ to $20$ when run on $12$
cores. This false negative rate is very small, but it compromises one of the
guarantees provided by standard bloom filters. In our implementation, we
resolve this race by utilizing the atomic builtin instruction \textit{
\_\_sync\_fetch\_and\_or}. It turns out that the use of this atomic instruction
does not have a big impact on performance. On simple benchmark with on $X_{S}$
with $50\%$ of all lookups outside the set $S$, the nonatomic multicore version
runs in $\approx 21.5$ seconds, and the atomic multicore versions in $\approx
21.8$ seconds.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "proposal"
%%% End: 

% LocalWords:  PageRank PageRanks vertices GraphLab parallelize parallelizing
% LocalWords:  nondeterministic nondeterminism runtime parallelized Seidel MIS
% LocalWords:  nonatomic parallelizes superlinear Luby's fluidanimate Plassmann
% LocalWords:  determinize multicore

